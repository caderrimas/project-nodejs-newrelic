## Project Nodejs Application and NewRelic Monitoring (APM - Application Performance Monitoring)

1. Directory Structure
     
        |---node_modules/
        |----Dockerfile
        |----index.js
        |----package.json
        |----package-lock.json
        |----README.md
        

2. Check Whether your Nodejs Application is Running or Not and Setup the Newrelic Prequisite...

      |---> use "**npm start**" to start the Application

  ![image](/uploads/31c4eb5228fe62e89ba42d5a5cde73b5/image.png)

  ## Steps to do for Nodejs Application

- [ ] Add the "**"newrelic": "latest"**" in package.json
- [ ] Add the "**require('newrelic');**" in starting page index.js
- [ ] "**ENV NEW_RELIC_NO_CONFIG_FILE=true**" in Dockerfile
- [ ] "**ENV NEW_RELIC_DISTRIBUTED_TRACING_ENABLED=true**" in Dockerfile
- [ ] "**ENV NEW_RELIC_LOG=stdout**" in Dockerfile

  ![image](/uploads/116866f005364a9ac1d5cf971164a531/image.png)

  ![image](/uploads/de62e1134e4daa83062fee50584fc36f/image.png)

  ![image](/uploads/01220ddcdb31d2ddc516c7c11726a29f/image.png)

## Dockerization: Dockerize the Nodejs Application
3. Dockerfile is a simple way to automate the process of building images 
```
FROM node:20.11-slim AS build

WORKDIR /app

COPY package*.json ./

RUN npm install

COPY . .

ENV NEW_RELIC_NO_CONFIG_FILE=true
ENV NEW_RELIC_DISTRIBUTED_TRACING_ENABLED=true
ENV NEW_RELIC_LOG=stdout

FROM node:20.11-slim

COPY --from=build /app/ .

EXPOSE 5005

CMD ["npm", "start"]

```

- [ ] Build the Dockerfile and Getting the Image
  ```
  docker build -t newrelic .

  ```
- [ ] Run the Docker Image
  ```
  docker run -d -e NEW_RELIC_LICENSE_KEY=77cd1d94c9d66aa6d72a28eaecdffa95FFFFNRAL -e NEW_RELIC_APP_NAME="local-myapp-newrelic" --name=newrelic-01 newrelic:latest
  
  ```
![image](/uploads/9f775e3b746a2de03cbd31a494eecd2e/image.png)