FROM node:20.11-slim AS build

WORKDIR /app

COPY package*.json ./

RUN npm install

COPY . .

ENV NEW_RELIC_NO_CONFIG_FILE=true
ENV NEW_RELIC_DISTRIBUTED_TRACING_ENABLED=true
ENV NEW_RELIC_LOG=stdout

FROM node:20.11-slim

COPY --from=build /app/ .

EXPOSE 5005

CMD ["npm", "start"]
